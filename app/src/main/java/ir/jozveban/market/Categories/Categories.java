package ir.jozveban.market.Categories;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Categories{

	@SerializedName("Categories")
	private List<CategoriesItem> categories;

	public void setCategories(List<CategoriesItem> categories){
		this.categories = categories;
	}

	public List<CategoriesItem> getCategories(){
		return categories;
	}

	@Override
 	public String toString(){
		return 
			"Categories{" + 
			"categories = '" + categories + '\'' + 
			"}";
		}
}