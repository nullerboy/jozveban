package ir.jozveban.market.Categories;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Embedded{

	@SerializedName("up")
	private List<UpItem> up;

	public void setUp(List<UpItem> up){
		this.up = up;
	}

	public List<UpItem> getUp(){
		return up;
	}

	@Override
 	public String toString(){
		return 
			"Embedded{" + 
			"up = '" + up + '\'' + 
			"}";
		}
}