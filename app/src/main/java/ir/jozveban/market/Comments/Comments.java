package ir.jozveban.market.Comments;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Comments{

	@SerializedName("Comments")
	private List<CommentsItem> comments;

	public void setComments(List<CommentsItem> comments){
		this.comments = comments;
	}

	public List<CommentsItem> getComments(){
		return comments;
	}
}