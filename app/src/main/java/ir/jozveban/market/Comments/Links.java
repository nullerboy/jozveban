package ir.jozveban.market.Comments;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Links{

	@SerializedName("self")
	private List<SelfItem> self;

	@SerializedName("collection")
	private List<CollectionItem> collection;

	@SerializedName("up")
	private List<UpItem> up;

	@SerializedName("curies")
	private List<CuriesItem> curies;

	@SerializedName("replies")
	private List<RepliesItem> replies;

	@SerializedName("version-history")
	private List<VersionHistoryItem> versionHistory;

	@SerializedName("author")
	private List<AuthorItem> author;

	@SerializedName("wp:term")
	private List<WpTermItem> wpTerm;

	@SerializedName("about")
	private List<AboutItem> about;

	@SerializedName("wp:featuredmedia")
	private List<WpFeaturedmediaItem> wpFeaturedmedia;

	@SerializedName("wp:attachment")
	private List<WpAttachmentItem> wpAttachment;

	public void setSelf(List<SelfItem> self){
		this.self = self;
	}

	public List<SelfItem> getSelf(){
		return self;
	}

	public void setCollection(List<CollectionItem> collection){
		this.collection = collection;
	}

	public List<CollectionItem> getCollection(){
		return collection;
	}

	public void setUp(List<UpItem> up){
		this.up = up;
	}

	public List<UpItem> getUp(){
		return up;
	}

	public void setCuries(List<CuriesItem> curies){
		this.curies = curies;
	}

	public List<CuriesItem> getCuries(){
		return curies;
	}

	public void setReplies(List<RepliesItem> replies){
		this.replies = replies;
	}

	public List<RepliesItem> getReplies(){
		return replies;
	}

	public void setVersionHistory(List<VersionHistoryItem> versionHistory){
		this.versionHistory = versionHistory;
	}

	public List<VersionHistoryItem> getVersionHistory(){
		return versionHistory;
	}

	public void setAuthor(List<AuthorItem> author){
		this.author = author;
	}

	public List<AuthorItem> getAuthor(){
		return author;
	}

	public void setWpTerm(List<WpTermItem> wpTerm){
		this.wpTerm = wpTerm;
	}

	public List<WpTermItem> getWpTerm(){
		return wpTerm;
	}

	public void setAbout(List<AboutItem> about){
		this.about = about;
	}

	public List<AboutItem> getAbout(){
		return about;
	}

	public void setWpFeaturedmedia(List<WpFeaturedmediaItem> wpFeaturedmedia){
		this.wpFeaturedmedia = wpFeaturedmedia;
	}

	public List<WpFeaturedmediaItem> getWpFeaturedmedia(){
		return wpFeaturedmedia;
	}

	public void setWpAttachment(List<WpAttachmentItem> wpAttachment){
		this.wpAttachment = wpAttachment;
	}

	public List<WpAttachmentItem> getWpAttachment(){
		return wpAttachment;
	}
}