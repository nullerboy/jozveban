package ir.jozveban.market.Download;

import com.google.gson.annotations.SerializedName;

public class Options{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("price_id")
	private int priceId;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setPriceId(int priceId){
		this.priceId = priceId;
	}

	public int getPriceId(){
		return priceId;
	}

	@Override
 	public String toString(){
		return 
			"Options{" + 
			"quantity = '" + quantity + '\'' + 
			",price_id = '" + priceId + '\'' + 
			"}";
		}
}