package ir.jozveban.market.Downloads;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Download{

	@SerializedName("fees")
	private List<Object> fees;

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("item_price")
	private int itemPrice;

	@SerializedName("subtotal")
	private int subtotal;

	@SerializedName("price")
	private int price;

	@SerializedName("name")
	private String name;

	@SerializedName("item_number")
	private ItemNumber itemNumber;

	@SerializedName("discount")
	private int discount;

	@SerializedName("tax")
	private int tax;

	@SerializedName("currency")
	private String currency;

	@SerializedName("fileUrl")
	private String fileUrl;

	@SerializedName("id")
	private int id;

	public void setFees(List<Object> fees){
		this.fees = fees;
	}

	public List<Object> getFees(){
		return fees;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setItemPrice(int itemPrice){
		this.itemPrice = itemPrice;
	}

	public int getItemPrice(){
		return itemPrice;
	}

	public void setSubtotal(int subtotal){
		this.subtotal = subtotal;
	}

	public int getSubtotal(){
		return subtotal;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setItemNumber(ItemNumber itemNumber){
		this.itemNumber = itemNumber;
	}

	public ItemNumber getItemNumber(){
		return itemNumber;
	}

	public void setDiscount(int discount){
		this.discount = discount;
	}

	public int getDiscount(){
		return discount;
	}

	public void setTax(int tax){
		this.tax = tax;
	}

	public int getTax(){
		return tax;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setFileUrl(String fileUrl){
		this.fileUrl = fileUrl;
	}

	public String getFileUrl(){
		return fileUrl;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Download{" + 
			"fees = '" + fees + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",item_price = '" + itemPrice + '\'' + 
			",subtotal = '" + subtotal + '\'' + 
			",price = '" + price + '\'' + 
			",name = '" + name + '\'' + 
			",item_number = '" + itemNumber + '\'' + 
			",discount = '" + discount + '\'' + 
			",tax = '" + tax + '\'' + 
			",currency = '" + currency + '\'' + 
			",fileUrl = '" + fileUrl + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}