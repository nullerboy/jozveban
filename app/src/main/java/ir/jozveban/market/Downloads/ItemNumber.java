package ir.jozveban.market.Downloads;

import com.google.gson.annotations.SerializedName;

public class ItemNumber{

	@SerializedName("quantity")
	private int quantity;

	@SerializedName("options")
	private Options options;

	@SerializedName("id")
	private int id;

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setOptions(Options options){
		this.options = options;
	}

	public Options getOptions(){
		return options;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"ItemNumber{" + 
			"quantity = '" + quantity + '\'' + 
			",options = '" + options + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}