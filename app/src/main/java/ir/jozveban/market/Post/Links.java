package ir.jozveban.market.Post;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Links{

	@SerializedName("replies")
	private List<RepliesItem> replies;

	@SerializedName("about")
	private List<AboutItem> about;

	@SerializedName("self")
	private List<SelfItem> self;

	@SerializedName("collection")
	private List<CollectionItem> collection;

	public void setReplies(List<RepliesItem> replies){
		this.replies = replies;
	}

	public List<RepliesItem> getReplies(){
		return replies;
	}

	public void setAbout(List<AboutItem> about){
		this.about = about;
	}

	public List<AboutItem> getAbout(){
		return about;
	}

	public void setSelf(List<SelfItem> self){
		this.self = self;
	}

	public List<SelfItem> getSelf(){
		return self;
	}

	public void setCollection(List<CollectionItem> collection){
		this.collection = collection;
	}

	public List<CollectionItem> getCollection(){
		return collection;
	}

	@Override
 	public String toString(){
		return 
			"Links{" + 
			"replies = '" + replies + '\'' + 
			",about = '" + about + '\'' + 
			",self = '" + self + '\'' + 
			",collection = '" + collection + '\'' + 
			"}";
		}
}