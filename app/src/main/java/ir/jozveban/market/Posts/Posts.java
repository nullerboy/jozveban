package ir.jozveban.market.Posts;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Posts{

	@SerializedName("Posts")
	private List<PostsItem> posts;

	public void setPosts(List<PostsItem> posts){
		this.posts = posts;
	}

	public List<PostsItem> getPosts(){
		return posts;
	}

	@Override
 	public String toString(){
		return 
			"Posts{" + 
			"posts = '" + posts + '\'' + 
			"}";
		}
}