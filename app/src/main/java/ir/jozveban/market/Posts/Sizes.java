package ir.jozveban.market.Posts;

import com.google.gson.annotations.SerializedName;

public class Sizes{

	@SerializedName("thumbnail")
	private Thumbnail thumbnail;

	@SerializedName("large")
	private Large large;

	@SerializedName("woocommerce_gallery_thumbnail")
	private WoocommerceGalleryThumbnail woocommerceGalleryThumbnail;

	@SerializedName("stthumb")
	private Stthumb stthumb;

	@SerializedName("woocommerce_single")
	private WoocommerceSingle woocommerceSingle;

	@SerializedName("woocommerce_thumbnail")
	private WoocommerceThumbnail woocommerceThumbnail;

	@SerializedName("medium")
	private Medium medium;

	@SerializedName("sqthumb")
	private Sqthumb sqthumb;

	@SerializedName("smthumb")
	private Smthumb smthumb;

	@SerializedName("medium_large")
	private MediumLarge mediumLarge;

	@SerializedName("verthumb")
	private Verthumb verthumb;

	@SerializedName("full")
	private Full full;

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	public void setLarge(Large large){
		this.large = large;
	}

	public Large getLarge(){
		return large;
	}

	public void setWoocommerceGalleryThumbnail(WoocommerceGalleryThumbnail woocommerceGalleryThumbnail){
		this.woocommerceGalleryThumbnail = woocommerceGalleryThumbnail;
	}

	public WoocommerceGalleryThumbnail getWoocommerceGalleryThumbnail(){
		return woocommerceGalleryThumbnail;
	}

	public void setStthumb(Stthumb stthumb){
		this.stthumb = stthumb;
	}

	public Stthumb getStthumb(){
		return stthumb;
	}

	public void setWoocommerceSingle(WoocommerceSingle woocommerceSingle){
		this.woocommerceSingle = woocommerceSingle;
	}

	public WoocommerceSingle getWoocommerceSingle(){
		return woocommerceSingle;
	}

	public void setWoocommerceThumbnail(WoocommerceThumbnail woocommerceThumbnail){
		this.woocommerceThumbnail = woocommerceThumbnail;
	}

	public WoocommerceThumbnail getWoocommerceThumbnail(){
		return woocommerceThumbnail;
	}

	public void setMedium(Medium medium){
		this.medium = medium;
	}

	public Medium getMedium(){
		return medium;
	}

	public void setSqthumb(Sqthumb sqthumb){
		this.sqthumb = sqthumb;
	}

	public Sqthumb getSqthumb(){
		return sqthumb;
	}

	public void setSmthumb(Smthumb smthumb){
		this.smthumb = smthumb;
	}

	public Smthumb getSmthumb(){
		return smthumb;
	}

	public void setMediumLarge(MediumLarge mediumLarge){
		this.mediumLarge = mediumLarge;
	}

	public MediumLarge getMediumLarge(){
		return mediumLarge;
	}

	public void setVerthumb(Verthumb verthumb){
		this.verthumb = verthumb;
	}

	public Verthumb getVerthumb(){
		return verthumb;
	}

	public void setFull(Full full){
		this.full = full;
	}

	public Full getFull(){
		return full;
	}

	@Override
 	public String toString(){
		return 
			"Sizes{" + 
			"thumbnail = '" + thumbnail + '\'' + 
			",large = '" + large + '\'' + 
			",woocommerce_gallery_thumbnail = '" + woocommerceGalleryThumbnail + '\'' + 
			",stthumb = '" + stthumb + '\'' + 
			",woocommerce_single = '" + woocommerceSingle + '\'' + 
			",woocommerce_thumbnail = '" + woocommerceThumbnail + '\'' + 
			",medium = '" + medium + '\'' + 
			",sqthumb = '" + sqthumb + '\'' + 
			",smthumb = '" + smthumb + '\'' + 
			",medium_large = '" + mediumLarge + '\'' + 
			",verthumb = '" + verthumb + '\'' + 
			",full = '" + full + '\'' + 
			"}";
		}
}