package ir.jozveban.market.Search;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Search{

	@SerializedName("Search")
	private List<SearchItem> search;

	public void setSearch(List<SearchItem> search){
		this.search = search;
	}

	public List<SearchItem> getSearch(){
		return search;
	}

	@Override
 	public String toString(){
		return 
			"Search{" + 
			"search = '" + search + '\'' + 
			"}";
		}
}