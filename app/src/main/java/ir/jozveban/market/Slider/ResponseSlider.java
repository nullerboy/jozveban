package ir.jozveban.market.Slider;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseSlider{

	@SerializedName("ResponseSlider")
	private List<ResponseSliderItem> responseSlider;

	public void setResponseSlider(List<ResponseSliderItem> responseSlider){
		this.responseSlider = responseSlider;
	}

	public List<ResponseSliderItem> getResponseSlider(){
		return responseSlider;
	}
}