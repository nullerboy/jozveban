package ir.jozveban.market.Slider;

import com.google.gson.annotations.SerializedName;

public class ResponseSliderItem{

	@SerializedName("image")
	private String image;

	@SerializedName("link")
	private String link;

	@SerializedName("title")
	private String title;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}
}