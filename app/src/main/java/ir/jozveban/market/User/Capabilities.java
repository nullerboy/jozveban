package ir.jozveban.market.User;

import com.google.gson.annotations.SerializedName;

public class Capabilities{

	@SerializedName("close_ticket")
	private boolean closeTicket;

	@SerializedName("attach_files")
	private boolean attachFiles;

	@SerializedName("wpas_user")
	private boolean wpasUser;

	@SerializedName("read")
	private boolean read;

	@SerializedName("create_ticket")
	private boolean createTicket;

	@SerializedName("view_ticket")
	private boolean viewTicket;

	@SerializedName("reply_ticket")
	private boolean replyTicket;

	@SerializedName("level_0")
	private boolean level0;

	public void setCloseTicket(boolean closeTicket){
		this.closeTicket = closeTicket;
	}

	public boolean isCloseTicket(){
		return closeTicket;
	}

	public void setAttachFiles(boolean attachFiles){
		this.attachFiles = attachFiles;
	}

	public boolean isAttachFiles(){
		return attachFiles;
	}

	public void setWpasUser(boolean wpasUser){
		this.wpasUser = wpasUser;
	}

	public boolean isWpasUser(){
		return wpasUser;
	}

	public void setRead(boolean read){
		this.read = read;
	}

	public boolean isRead(){
		return read;
	}

	public void setCreateTicket(boolean createTicket){
		this.createTicket = createTicket;
	}

	public boolean isCreateTicket(){
		return createTicket;
	}

	public void setViewTicket(boolean viewTicket){
		this.viewTicket = viewTicket;
	}

	public boolean isViewTicket(){
		return viewTicket;
	}

	public void setReplyTicket(boolean replyTicket){
		this.replyTicket = replyTicket;
	}

	public boolean isReplyTicket(){
		return replyTicket;
	}

	public void setLevel0(boolean level0){
		this.level0 = level0;
	}

	public boolean isLevel0(){
		return level0;
	}

	@Override
 	public String toString(){
		return 
			"Capabilities{" + 
			"close_ticket = '" + closeTicket + '\'' + 
			",attach_files = '" + attachFiles + '\'' + 
			",wpas_user = '" + wpasUser + '\'' + 
			",read = '" + read + '\'' + 
			",create_ticket = '" + createTicket + '\'' + 
			",view_ticket = '" + viewTicket + '\'' + 
			",reply_ticket = '" + replyTicket + '\'' + 
			",level_0 = '" + level0 + '\'' + 
			"}";
		}
}