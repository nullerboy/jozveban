package ir.jozveban.market.User;

import com.google.gson.annotations.SerializedName;

public class ExtraCapabilities{

	@SerializedName("wpas_user")
	private boolean wpasUser;

	public void setWpasUser(boolean wpasUser){
		this.wpasUser = wpasUser;
	}

	public boolean isWpasUser(){
		return wpasUser;
	}

	@Override
 	public String toString(){
		return 
			"ExtraCapabilities{" + 
			"wpas_user = '" + wpasUser + '\'' + 
			"}";
		}
}