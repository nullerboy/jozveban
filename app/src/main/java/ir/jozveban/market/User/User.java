package ir.jozveban.market.User;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class User{

	@SerializedName("capabilities")
	private Capabilities capabilities;

	@SerializedName("avatar_urls")
	private AvatarUrls avatarUrls;

	@SerializedName("roles")
	private List<String> roles;

	@SerializedName("link")
	private String link;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("description")
	private String description;

	@SerializedName("locale")
	private String locale;

	@SerializedName("url")
	private String url;

	@SerializedName("name")
	private String name;

	@SerializedName("nickname")
	private String nickname;

	@SerializedName("id")
	private int id;

	@SerializedName("registered_date")
	private String registeredDate;

	@SerializedName("extra_capabilities")
	private ExtraCapabilities extraCapabilities;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("slug")
	private String slug;

	@SerializedName("username")
	private String username;

	public void setCapabilities(Capabilities capabilities){
		this.capabilities = capabilities;
	}

	public Capabilities getCapabilities(){
		return capabilities;
	}

	public void setAvatarUrls(AvatarUrls avatarUrls){
		this.avatarUrls = avatarUrls;
	}

	public AvatarUrls getAvatarUrls(){
		return avatarUrls;
	}

	public void setRoles(List<String> roles){
		this.roles = roles;
	}

	public List<String> getRoles(){
		return roles;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setLocale(String locale){
		this.locale = locale;
	}

	public String getLocale(){
		return locale;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setRegisteredDate(String registeredDate){
		this.registeredDate = registeredDate;
	}

	public String getRegisteredDate(){
		return registeredDate;
	}

	public void setExtraCapabilities(ExtraCapabilities extraCapabilities){
		this.extraCapabilities = extraCapabilities;
	}

	public ExtraCapabilities getExtraCapabilities(){
		return extraCapabilities;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setSlug(String slug){
		this.slug = slug;
	}

	public String getSlug(){
		return slug;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public String getUsername(){
		return username;
	}

	@Override
 	public String toString(){
		return 
			"User{" + 
			"capabilities = '" + capabilities + '\'' + 
			",avatar_urls = '" + avatarUrls + '\'' + 
			",roles = '" + roles + '\'' + 
			",link = '" + link + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",description = '" + description + '\'' + 
			",locale = '" + locale + '\'' + 
			",url = '" + url + '\'' + 
			",name = '" + name + '\'' + 
			",nickname = '" + nickname + '\'' + 
			",id = '" + id + '\'' + 
			",registered_date = '" + registeredDate + '\'' + 
			",extra_capabilities = '" + extraCapabilities + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",email = '" + email + '\'' + 
			",slug = '" + slug + '\'' + 
			",username = '" + username + '\'' + 
			"}";
		}
}