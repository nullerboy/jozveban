package ir.jozveban.market.activities;

import static ir.jozveban.market.util.Params.BASE_URL;
import static ir.jozveban.market.util.Params.BASE_URL_EDD;
import static ir.jozveban.market.util.Params.MAIN_BASE_URL;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import dmax.dialog.SpotsDialog;
import ir.jozveban.market.R;
import ir.jozveban.market.util.APi;
import ir.jozveban.market.util.EDD;
import ir.jozveban.market.util.Pref;

import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import ir.jozveban.market.util.SliderAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseActivity extends AppCompatActivity {

    public Context context;
    public Retrofit retrofit;
    public APi aPi;
    public String TAG = "Activity";
    public Pref pref;
    public Retrofit retrofitEdd;
    public EDD edd;
    public AlertDialog loadingDialog;

    public SliderAPI sliderAPI;
    public Retrofit retrofitSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        context = BaseActivity.this;

        pref = new Pref(context);
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitEdd = new Retrofit.Builder()
                .baseUrl(BASE_URL_EDD)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        retrofitSlider = new Retrofit.Builder()
                .baseUrl(MAIN_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        aPi = retrofit.create(APi.class);
        edd = retrofitEdd.create(EDD.class);
        sliderAPI = retrofitSlider.create(SliderAPI.class);

        loadingDialog = new SpotsDialog.Builder().setContext(context)
                .setMessage("شکیبا باشید").build();


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }
}