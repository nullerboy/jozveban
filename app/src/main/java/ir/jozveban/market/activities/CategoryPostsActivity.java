package ir.jozveban.market.activities;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ir.jozveban.market.Posts.PostsItem;
import ir.jozveban.market.R;
import ir.jozveban.market.adapters.OnEndReached;
import ir.jozveban.market.adapters.OnRecyclerItemClicked;
import ir.jozveban.market.adapters.PostsAdapter;
import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPostsActivity extends BaseActivity {

    Bundle bundle;

    @BindView(R.id.text_title)
    TextView textTitle;

    @BindView(R.id.recycler_posts)
    RecyclerView recyclerPosts;

    @BindView(R.id.loading_posts)
    SpinKitView loadingPosts;

    private int pageNumber = 1;
    private int perPage = 12;
    private int catId = 0;
    PostsAdapter adapter;
    List<PostsItem> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_posts);
        ButterKnife.bind(this);

        posts = new ArrayList<>();
        adapter = new PostsAdapter(context, posts);
        recyclerPosts.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerPosts.setAdapter(adapter);

        adapter.setOnEndReached(new OnEndReached() {
            @Override
            public void OnReached() {
                System.err.println("pageNumber : "+pageNumber );
                if (catId == 0)
                    getPosts(perPage, pageNumber);
                else
                    getPosts(perPage, pageNumber, catId);

            }
        });
        adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
            @Override
            public void OnClick(int position) {
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("postId", posts.get(position).getId());
                intent.putExtra("postTitle", posts.get(position).getTitle().getRendered());
                startActivity(intent);
            }
        });

        if (getIntent().getExtras() != null) {
            bundle = getIntent().getExtras();
            catId = bundle.getInt("catId");
            textTitle.setText(bundle.getString("catName"));
            if (catId == 0) {
                getPosts(perPage, pageNumber);
            } else {
                getPosts(perPage, pageNumber, catId);
            }
        }


    }

    public void getPosts(int perPage, int pageNum, int catId) {
        loadingDialog.show();
        aPi.getPostsByCategory(perPage, pageNum, catId).enqueue(new Callback<List<PostsItem>>() {

            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                if (response.code() == 200) {
                    posts.addAll(response.body());
                    adapter.notifyItemInserted(posts.size());
                    pageNumber++;
                }
                loadingDialog.cancel();

            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                loadingDialog.cancel();
            }
        });
    }

    public void getPosts(int perPage, int pageNum) {
        loadingDialog.show();
        aPi.getPosts(perPage, pageNum).enqueue(new Callback<List<PostsItem>>() {

            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                if (response.code() == 200) {
                    posts.addAll(response.body());
                    adapter.notifyItemInserted(posts.size());
                    pageNumber++;
                }
                loadingDialog.cancel();

            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                loadingDialog.cancel();
            }
        });
    }

}