package ir.jozveban.market.activities;

import static ir.jozveban.market.util.Params.BASE_URL;
import static ir.jozveban.market.util.Params.BASE_URL_EDD;
import static ir.jozveban.market.util.Params.KEY;
import static ir.jozveban.market.util.Params.TOKEN;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.Download.DownloadsItem;
import ir.jozveban.market.Downloads.Download;
import ir.jozveban.market.R;
import ir.jozveban.market.adapters.DownloadsAdapter;
import ir.jozveban.market.adapters.OnRecyclerItemClicked;
import ir.jozveban.market.util.APi;
import ir.jozveban.market.util.EDD;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryActivity extends BaseActivity {

    @BindView(R.id.loading_downloads)
    SpinKitView loading;

    @BindView(R.id.recycler_downloads)
    RecyclerView recycler;

    List<DownloadsItem> downloadList;
    DownloadsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        downloadList = new ArrayList<>();
        adapter = new DownloadsAdapter(context, downloadList);

        recycler.setLayoutManager(new LinearLayoutManager(context));
        recycler.setAdapter(adapter);

        adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
            @Override
            public void OnClick(int position) {

                if (downloadList.get(position).getFileUrl()!=null){
                    String fileUrl = downloadList.get(position).getFileUrl();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(fileUrl));
                    startActivity(intent);
                }

            }
        });

        loadingDialog.show();
        edd.getDownloads(TOKEN, KEY, pref.getUserId())
                .enqueue(new Callback<List<DownloadsItem>>() {
                    @Override
                    public void onResponse(Call<List<DownloadsItem>> call, Response<List<DownloadsItem>> response) {

                        System.err.println(call.request().url());

                        if (response.code() == 200) {

                            downloadList.addAll(response.body());
                            adapter.notifyDataSetChanged();
                        }

                        loadingDialog.cancel();
//                        loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<List<DownloadsItem>> call, Throwable t) {
                        System.err.println(t.getLocalizedMessage());
                        System.err.println(call.request().url());

                    }
                });

    }
}