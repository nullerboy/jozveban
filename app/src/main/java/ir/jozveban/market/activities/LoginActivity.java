package ir.jozveban.market.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ir.jozveban.market.Me.Me;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.jozveban.market.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.button_submit)
    Button buttonSubmit;
    @BindView(R.id.button_register)
    Button buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.button_submit,
            R.id.button_register})
    void OnClick(View view) {
        int id = view.getId();

        if (id == R.id.button_submit) {

            String email = inputEmail.getText().toString();
            String password = inputPassword.getText().toString();

            if (email.isEmpty() || password.isEmpty()) {
                Snackbar.make(view, "فیلد ها را پر کنید", BaseTransientBottomBar.LENGTH_SHORT)
                        .show();
            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Snackbar.make(view, "ایمیل وارد شده معتبر نیست", BaseTransientBottomBar.LENGTH_SHORT)
                        .show();
            } else {
                Snackbar.make(view, "درحال ورود به حساب کاربری", BaseTransientBottomBar.LENGTH_LONG)
                        .show();
                login(email, password);
            }

        } else if (id == R.id.button_register) {
            startActivity(new Intent(context, RegisterActivity.class));
            finish();
        }
    }

    public void login(String username, String password) {
        loadingDialog.show();
        String cred = Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + cred);
        aPi.login(headers).enqueue(new Callback<Me>() {
            @Override
            public void onResponse(Call<Me> call, Response<Me> response) {
                if (response.code() == 200) {
                    Toast.makeText(context, "خوش آمدید", Toast.LENGTH_SHORT)
                            .show();
                    pref.setUserEmail(username);
                    pref.setUserCredential(cred);
                    pref.setUserId(response.body().getId() + "");
                    System.err.println(pref.getUserId());

                    finish();
                } else {
                    Toast.makeText(context, "نام کاربری یا رمزعبور اشتباه است", Toast.LENGTH_SHORT)
                            .show();
                }
                loadingDialog.cancel();
                System.err.println(response.code());
            }

            @Override
            public void onFailure(Call<Me> call, Throwable t) {
                loadingDialog.cancel();
            }
        });

    }
}