package ir.jozveban.market.activities;

import static ir.jozveban.market.util.Params.CAT_ID_1;
import static ir.jozveban.market.util.Params.CAT_ID_2;
import static ir.jozveban.market.util.Params.FEATURED_CATEGORY_ID;
import static ir.jozveban.market.util.Params.LINK_JOZVEBAN_APP;
import static ir.jozveban.market.util.Params.LINK_OTHER_APPS;
import static ir.jozveban.market.util.Params.LINK_SUPPORT;
import static ir.jozveban.market.util.Params.LINK_TERMS;
import static ir.jozveban.market.util.Params.MAIN_MIX_PARENT_CATEGORY_ID;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ir.jozveban.market.Categories.CategoriesItem;
import ir.jozveban.market.Posts.PostsItem;
import ir.jozveban.market.R;
import ir.jozveban.market.Slider.ResponseSliderItem;
import ir.jozveban.market.adapters.CategoriesAdapter;
import ir.jozveban.market.adapters.GetRecycler;
import ir.jozveban.market.adapters.MainMixedAdapter;
import ir.jozveban.market.adapters.OnEndReached;
import ir.jozveban.market.adapters.OnRecyclerItemClicked;
import ir.jozveban.market.adapters.PostsAdapter;

import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.navigation.NavigationView;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ir.jozveban.market.adapters.SliderAdapterExample;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("NonConstantResourceId")
public class MainActivity extends BaseActivity {

    @BindView(R.id.loading_cats)
    SpinKitView loadingCats;

    @BindView(R.id.loading_recent)
    SpinKitView loadingRecent;

    @BindView(R.id.loading_1)
    SpinKitView loading1;

    @BindView(R.id.loading_2)
    SpinKitView loading2;

    @BindView(R.id.recycler_cats)
    RecyclerView recyclerCats;

    @BindView(R.id.recycler_recent)
    RecyclerView recyclerRecent;

    @BindView(R.id.recycler_1)
    RecyclerView recycler1;

    @BindView(R.id.recycler_2)
    RecyclerView recycler2;

    @BindView(R.id.recycler_mixed_posts)
    RecyclerView recyclerMixedPosts;

    @BindView(R.id.button_profile)
    Button buttonProfile;

    @BindView(R.id.button_dialog)
    Button buttonDialog;

    @BindView(R.id.button_search)
    Button buttonSearch;

    @BindView(R.id.navigation_main)
    NavigationView navigationView;

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    @BindView(R.id.drawer)
    DrawerLayout drawerLayout;

    ActionBarDrawerToggle toggle;

    @BindView(R.id.imageSlider)
    SliderView sliderView;
    private SliderAdapterExample adapter;

    @Override
    protected void onResume() {
        super.onResume();
        View headerLayout = navigationView.getHeaderView(0); // 0-index header
        TextView textView = headerLayout.findViewById(R.id.text_1);
        ImageButton buttonLogout = headerLayout.findViewById(R.id.button_logout);
        textView.setText(pref.getUserEmail().isEmpty() ? "کاربر میهمان" : pref.getUserEmail());
        buttonLogout.setVisibility(pref.getUserId().isEmpty() ? View.GONE : View.VISIBLE);

        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(context)
                        .setTitle("خروج از حساب")
                        .setMessage("از حساب کاربری خود خارج می شوید؟")
                        .setPositiveButton("بله", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                pref.clearPrefs();
                                startActivity(new Intent(context, SplashActivity.class));
                                finish();
                            }
                        })
                        .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        })
                        .create();
                dialog.show();
            }
        });
    }

    private long downloadID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        toggle.syncState();

        loadSlider();
        getCategories();
        getPosts(12, 1);
        getPostsList1(6, 1, CAT_ID_1);
        getPostsList2(6, 1, CAT_ID_2);
//        getNewPosts(6,1,NEW_POSTS_CATEGORY_ID);
//        getFeaturedPosts(6, 1, FEATURED_CATEGORY_ID);
//        getCategoriesByParent(MAIN_MIX_PARENT_CATEGORY_ID);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (R.id.menu_support == id) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_SUPPORT)));
                } else if (R.id.menu_terms == id) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_TERMS)));
                } else if (R.id.menu_apps == id) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_OTHER_APPS)));
                } else if (R.id.menu_history == id) {

                    if (pref.getUserId().isEmpty()) {
                        startActivity(new Intent(context, LoginActivity.class));
                    } else {
                        startActivity(new Intent(context, HistoryActivity.class));
                    }

                }

                return false;
            }
        });


    }


    public void loadSlider() {
        sliderAPI.getSlider().enqueue(new Callback<List<ResponseSliderItem>>() {
            @Override
            public void onResponse(Call<List<ResponseSliderItem>> call, Response<List<ResponseSliderItem>> response) {
                adapter = new SliderAdapterExample(context, response.body());
                sliderView.setSliderAdapter(adapter);
                sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
                sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
                sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
                sliderView.setIndicatorSelectedColor(Color.WHITE);
                sliderView.setIndicatorUnselectedColor(Color.GRAY);
                sliderView.setScrollTimeInSec(3);
                sliderView.setAutoCycle(true);
                sliderView.startAutoCycle();

            }

            @Override
            public void onFailure(Call<List<ResponseSliderItem>> call, Throwable t) {

            }
        });

        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                Log.i("GGG", "onIndicatorClicked: " + sliderView.getCurrentPagePosition());
            }
        });

    }

    @OnClick({
            R.id.button_see_more_recent,
            R.id.button_see_more_1,
            R.id.button_see_more_2,
            R.id.button_profile,
            R.id.button_search,
            R.id.button_dialog
    })
    void OnClick(View view) {
        int id = view.getId();

        if (R.id.button_see_more_recent == id) {

            Intent intent = new Intent(context, CategoryPostsActivity.class);
            intent.putExtra("catId", 0);
            intent.putExtra("catName", "جدیدترین جزوه‌های دانشگاهی");
            startActivity(intent);

        } else if (R.id.button_see_more_1 == id) {

            Intent intent = new Intent(context, CategoryPostsActivity.class);
            intent.putExtra("catId", CAT_ID_1);
            intent.putExtra("catName", "جدبدترین پاورپوینت های دانشگاهی");
            startActivity(intent);

        } else if (R.id.button_see_more_2 == id) {

            Intent intent = new Intent(context, CategoryPostsActivity.class);
            intent.putExtra("catId", CAT_ID_2);
            intent.putExtra("catName", "جدید ترین نمونه سوالات");
            startActivity(intent);

        } else if (R.id.button_profile == id) {

            if (pref.getUserCredential().isEmpty()) {
                startActivity(new Intent(context, LoginActivity.class));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(pref.getUserEmail());
                builder.setItems(new String[]{"تاریخچه دانلودها", "خروج از حساب"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == 0) {
                            startActivity(new Intent(context, HistoryActivity.class));
                        } else if (i == 1) {
                            pref.clearPrefs();
                        }
                    }
                });
                builder.show();
            }

        } else if (R.id.button_search == id) {

            startActivity(new Intent(context, SearchActivity.class));

        } else if (R.id.button_dialog == id) {

            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setItems(new String[]{"پشتیبانی", "قوانین استفاده از برنامه", "سایر برنامه ها"}, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    if (i == 0) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_SUPPORT)));
                    } else if (i == 1) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_TERMS)));
                    } else if (i == 2) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_OTHER_APPS)));
                    }

                }
            });
            dialog.show();

        }
    }

    public void getCategories() {
        loadingCats.setVisibility(View.VISIBLE);
        aPi.getCategoriesByParent(MAIN_MIX_PARENT_CATEGORY_ID).enqueue(new Callback<List<CategoriesItem>>() {
            @Override
            public void onResponse(Call<List<CategoriesItem>> call, Response<List<CategoriesItem>> response) {

                CategoriesAdapter adapter = new CategoriesAdapter(response.body(), context);
                recyclerCats.setAdapter(adapter);
                recyclerCats.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Intent intent = new Intent(context, CategoryPostsActivity.class);
                        intent.putExtra("catId", response.body().get(position).getId());
                        intent.putExtra("catName", response.body().get(position).getName());
                        startActivity(intent);
                    }
                });

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });
                loadingCats.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<CategoriesItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                loadingCats.setVisibility(View.VISIBLE);
                Toast.makeText(context, "خطای برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();
                Toast.makeText(context, "تلاش برای اتصال دوباره", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getPosts(int perPage, int pageNum) {
        loadingRecent.setVisibility(View.VISIBLE);
        aPi.getPosts(perPage, pageNum).enqueue(new Callback<List<PostsItem>>() {
            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                PostsAdapter adapter = new PostsAdapter(context, response.body());
                recyclerRecent.setAdapter(adapter);
                recyclerRecent.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Log.e(TAG, "OnClick: " + "postId:" + response.body().get(position).getId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postId", response.body().get(position).getId());
                        intent.putExtra("postTitle", response.body().get(position).getTitle().getRendered());
                        startActivity(intent);
                    }
                });

                loadingRecent.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                System.err.println(call.request().url());
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    public void getNewPosts(int perPage, int pageNum, int categoryId) {
        loading1.setVisibility(View.VISIBLE);
        aPi.getPostsByCategory(perPage, pageNum, categoryId).enqueue(new Callback<List<PostsItem>>() {
            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                Log.e(TAG, "onResponse: " + call.request().url());
                PostsAdapter adapter = new PostsAdapter(context, response.body());
                recycler1.setAdapter(adapter);
                recycler1.setLayoutManager(new GridLayoutManager(context, 2));

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Log.e(TAG, "OnClick: " + "postId:" + response.body().get(position).getId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postId", response.body().get(position).getId());
                        intent.putExtra("postTitle", response.body().get(position).getTitle().getRendered());
                        startActivity(intent);
                    }
                });

                loading1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    public void getPostsList1(int perPage, int pageNum, int featuredCategoryId) {
        loading1.setVisibility(View.VISIBLE);
        aPi.getPostsByCategory(perPage, pageNum, featuredCategoryId).enqueue(new Callback<List<PostsItem>>() {
            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                Log.e(TAG, "getPostsList1: " + call.request().url());
                PostsAdapter adapter = new PostsAdapter(context, response.body());
                recycler1.setAdapter(adapter);
                recycler1.setLayoutManager(new GridLayoutManager(context, 2));

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Log.e(TAG, "OnClick: " + "postId:" + response.body().get(position).getId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postId", response.body().get(position).getId());
                        intent.putExtra("postTitle", response.body().get(position).getTitle().getRendered());
                        startActivity(intent);
                    }
                });

                loading1.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    public void getPostsList2(int perPage, int pageNum, int featuredCategoryId) {
        loading2.setVisibility(View.VISIBLE);
        aPi.getPostsByCategory(perPage, pageNum, featuredCategoryId).enqueue(new Callback<List<PostsItem>>() {
            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                Log.e(TAG, "getPostsList2: " + call.request().url());
                PostsAdapter adapter = new PostsAdapter(context, response.body());
                recycler2.setAdapter(adapter);
                recycler2.setLayoutManager(new GridLayoutManager(context, 2));

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Log.e(TAG, "OnClick: " + "postId:" + response.body().get(position).getId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postId", response.body().get(position).getId());
                        intent.putExtra("postTitle", response.body().get(position).getTitle().getRendered());
                        startActivity(intent);
                    }
                });

                loading2.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    public void getPostsByCategory(int perPage, int pageNum, int categoryId, RecyclerView recyclerView) {
//        loadingFeaturedPosts.setVisibility(View.VISIBLE);
        aPi.getPostsByCategory(perPage, pageNum, categoryId).enqueue(new Callback<List<PostsItem>>() {
            @Override
            public void onResponse(Call<List<PostsItem>> call, Response<List<PostsItem>> response) {
                PostsAdapter adapter = new PostsAdapter(context, response.body());
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

                adapter.setOnEndReached(new OnEndReached() {
                    @Override
                    public void OnReached() {

                    }
                });

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Log.e(TAG, "OnClick: " + "postId:" + response.body().get(position).getId());
                        Intent intent = new Intent(context, PostActivity.class);
                        intent.putExtra("postId", response.body().get(position).getId());
                        intent.putExtra("postTitle", response.body().get(position).getTitle().getRendered());
                        startActivity(intent);
                    }
                });

//                loadingFeaturedPosts.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<PostsItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());

            }
        });
    }

    public void getCategoriesByParent(int parentId) {

        aPi.getCategoriesByParent(parentId).enqueue(new Callback<List<CategoriesItem>>() {
            @Override
            public void onResponse(Call<List<CategoriesItem>> call, Response<List<CategoriesItem>> response) {

                MainMixedAdapter adapter = new MainMixedAdapter(context, response.body());
                recyclerMixedPosts.setLayoutManager(new LinearLayoutManager(context));
                recyclerMixedPosts.setAdapter(adapter);

                adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
                    @Override
                    public void OnClick(int position) {
                        Intent intent = new Intent(context, CategoryPostsActivity.class);
                        intent.putExtra("catId", response.body().get(position).getId());
                        intent.putExtra("catName", response.body().get(position).getName());
                        startActivity(intent);
                    }
                });

                adapter.getRecycler(new GetRecycler() {
                    @Override
                    public void getRecyclerView(RecyclerView recyclerView, int position) {
                        Log.e(TAG, "getRecyclerView: " + recyclerView.getId() + " -> " + position);
                        getPostsByCategory(12,
                                1,
                                response.body().get(position).getId(),
                                recyclerView);
                    }
                });

            }

            @Override
            public void onFailure(Call<List<CategoriesItem>> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
            }
        });

    }

    @Override
    public void onBackPressed() {

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setTitle("خروج از برنامه")
                .setMessage("با امتیاز و نظر خود در کافه بازار ما را حمایت کنید")
                .setPositiveButton("امتیاز میدم⭐", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(LINK_JOZVEBAN_APP)));
                        finish();
                    }
                })
                .setNegativeButton("خروج از برنامه📴", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .create();
        dialog.show();
    }
}