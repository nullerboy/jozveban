package ir.jozveban.market.activities;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ir.jozveban.market.Comments.CommentsItem;
import ir.jozveban.market.Post.Post;

import com.github.ybq.android.spinkit.SpinKitView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;
import ir.jozveban.market.adapters.CommentsAdapter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostActivity extends BaseActivity {

    Bundle bundle;

    @BindView(R.id.text_title)
    TextView textTitle;
    @BindView(R.id.text_author_name)
    TextView textAuthorName;
    @BindView(R.id.text_author_bio)
    TextView textAuthorBio;
    @BindView(R.id.image_author)
    ImageView imageAuthor;
    @BindView(R.id.webview_content)
    WebView webViewContent;
    @BindView(R.id.scrollview_main)
    NestedScrollView scrollMain;
    @BindView(R.id.loading_post)
    SpinKitView loading;
    @BindView(R.id.button_buy)
    Button buttonBuy;

    @BindView(R.id.button_submit_comment)
    Button submitComment;

    @BindView(R.id.input_comment_name)
    EditText commentName;

    @BindView(R.id.input_comment_email)
    EditText commentEmail;

    @BindView(R.id.input_comment_content)
    EditText commentContent;

    @BindView(R.id.recycler_comments)
    RecyclerView recyclerComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        ButterKnife.bind(this);

        scrollMain.setVisibility(View.GONE);
        webViewContent.setWebChromeClient(new WebChromeClient());

        commentEmail.setText(pref.getUserEmail());

        if (getIntent().getExtras() != null) {

            bundle = getIntent().getExtras();

            textTitle.setText(bundle.getString("postTitle"));
            int postId = bundle.getInt("postId");
            getPost(postId);
            getComments(postId);

            submitComment.setOnClickListener(v -> {
                String content = commentContent.getText().toString();
                String email = commentEmail.getText().toString();
                String author = commentName.getText().toString();

                if (content.trim().equals("") ||
                        email.trim().equals("") ||
                        author.trim().equals("")) {
                    Toast.makeText(context, "فیلدها را پر کنید", Toast.LENGTH_SHORT).show();
                } else {
                    submitComment(pref.getUserCredential(), postId, author, email, content);
                }


            });
        }

    }

    public void getPost(int postId) {
        loadingDialog.show();

        aPi.getPost(postId).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                Log.d(TAG, "onResponse: " + call.request().url());

                Post post = response.body();

                textTitle.setText(post.getTitle().getRendered());

                String content = "<body dir=\"rtl\">" +
                        post.getContent().getRendered().replace("&#8230;", "") +
                        "</body>";

                if (content.contains("<form")) {
                    String removedForm =
                            content.substring(0, content.indexOf("<form"));
                    Log.e(TAG, "onResponse: " + removedForm);
                    webViewContent.loadDataWithBaseURL(null,
                            getStyledFont(removedForm),
                            "text/html",
                            "UTF-8",
                            "");


                    Document document = Jsoup.parse(content);
                    Elements div = document.select("div");
                    Elements e1 = div.select("a");
                    Elements e2 = div.select("input");
                    Log.e("text", e1.get(0).text());
                    Log.e("text", e2.get(0).attr("data-download-id"));

                    buttonBuy.setText(e1.get(0).text().isEmpty() ? "خرید و دانلود فایل" : e1.get(0).text());
                    buttonBuy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, WebViewActivity.class);
                            intent.putExtra("title", post.getTitle().getRendered().toString());
                            intent.putExtra("link", post.getLink());
                            intent.putExtra("downloadId", e2.get(0).attr("data-download-id"));
                            startActivity(intent);
                        }
                    });

                } else {
                    buttonBuy.setVisibility(View.GONE);
                    Log.d(TAG, getStyledFont(content));
                    loadWebView(getStyledFont(content));
                }
                textAuthorName.setText(post.getEmbedded().getAuthor().get(0).getName());
                textAuthorBio.setText(post.getEmbedded().getAuthor().get(0).getDescription());

                Picasso.get()
                        .load(post.getEmbedded().getAuthor().get(0).getAvatarUrls().getJsonMember96())
                        .into(imageAuthor);

                loadingDialog.cancel();
                scrollMain.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getLocalizedMessage());
                loadingDialog.cancel();
            }
        });

    }

    public void loadWebView(String data) {

        Toast.makeText(context, "درحال بارگذاری", Toast.LENGTH_SHORT).show();
        webViewContent.getSettings().setJavaScriptEnabled(true);
        webViewContent.getSettings().setLoadsImagesAutomatically(true);
        webViewContent.getSettings().setDomStorageEnabled(true);
        webViewContent.setLongClickable(true);
        webViewContent.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            startDownload(url);
        });

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.setAcceptThirdPartyCookies(webViewContent, true);
        cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
            @Override
            public void onReceiveValue(Boolean aBoolean) {

            }
        });

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("download_id", "107493");
            jsonObject.put("edd_redirect_to_checkout", "1");
            jsonObject.put("edd_action", "add_to_cart");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        webViewContent.setWebViewClient(new WebViewClient());

        webViewContent.loadDataWithBaseURL(null,
                data,
                "text/html",
                "UTF-8",
                "");


    }

    public static String getStyledFont(String html) {
        String head = "<head><style>img{max-width: 100%; width:auto; height: auto;}</style></head>";
        boolean addBodyStart = !html.toLowerCase().contains("<body>");
        boolean addBodyEnd = !html.toLowerCase().contains("</body");
        String res = "<html dir=\"rtl\" lang=\"\"><style type=\"text/css\">@font-face {font-family: CustomFont;" +
                "src: url(\"file:///android_asset/fonts/iransans.ttf\")}" +
                "body {font-family: CustomFont;font-size: small;text-align: justify;}</style>" + head +
                (addBodyStart ? "<body>" : "") + html + (addBodyEnd ? "</body></html>" : "");
        res = res.replace("<video ", "<video width=\"100%\" ");
        return res;
    }

    public void startDownload(String url) {
//        Uri uri = Uri.parse(url);
        Log.e(TAG, "startDownload: " + url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        fileName = fileName.substring(0, 1).toUpperCase() + fileName.substring(1);
//
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)// Uri of the destination file
                .setTitle(fileName)// Title of the Download Notification
                .setDescription("دانلود از جزوه بان")// Description of the Download Notification
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);// e
    }

    public void getComments(Integer postId) {
        aPi.getComments(postId).enqueue(new Callback<List<CommentsItem>>() {
            @Override
            public void onResponse(Call<List<CommentsItem>> call, Response<List<CommentsItem>> response) {
                List<CommentsItem> comments = response.body();

                CommentsAdapter adapter = new CommentsAdapter(comments);
                recyclerComments.setLayoutManager(new LinearLayoutManager(context));
                recyclerComments.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<CommentsItem>> call, Throwable t) {
            }
        });
    }

    public void submitComment(String cred, int postId, String author, String email, String content) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + cred);
        aPi.submitComment(headers, postId, author, email, content).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d("submitComment", "Code " + response.code());
                Toast.makeText(context, "دیدگاه با موفقیت برای مدیر ارسال شد", Toast.LENGTH_LONG).show();
                commentName.setText("");
                commentContent.setText("");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}