package ir.jozveban.market.activities;

import android.os.Bundle;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ir.jozveban.market.R;
import ir.jozveban.market.User.User;
import com.google.gson.Gson;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password1)
    EditText inputPassword1;
    @BindView(R.id.input_password2)
    EditText inputPassword2;
    @BindView(R.id.button_submit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = inputEmail.getText().toString();
                String pass1 = inputPassword1.getText().toString();
                String pass2 = inputPassword2.getText().toString();

                if (email.isEmpty() || pass1.isEmpty() || pass2.isEmpty()) {
                    Toast.makeText(context, "تمامی فیلدها را پر کنید", Toast.LENGTH_SHORT)
                            .show();
                } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    Toast.makeText(context, "ایمیل معتبر نیست", Toast.LENGTH_SHORT)
                            .show();
                } else if (!pass1.equals(pass2)) {
                    Toast.makeText(context, "رمزهای وارد شده مشابه نیستند", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    register(email, email, pass1);
                }


            }
        });


    }

    public void register(String email, String username, String password) {
        loadingDialog.show();
        aPi.register(email, username, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                String cred = Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
                int code = response.code();

                if (code == 201) {
                    try {
                        User user = new Gson().fromJson(response.body().string(), User.class);
                        pref.setUserId(user.getId() + "");
                        pref.setUserCredential(cred);
                        pref.setUserEmail(email);
                        Toast.makeText(context,"ثبت نام با موفقیت انجام شد",Toast.LENGTH_SHORT)
                                .show();
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 500) {
                    Toast.makeText(context, "ایمیل یا نام کاربری قبلا ثبت شده است", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    Toast.makeText(context, "خطایی رخ داده", Toast.LENGTH_SHORT)
                            .show();

                }
                loadingDialog.cancel();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(context, "خطای برقراری ارتباط", Toast.LENGTH_SHORT)
                        .show();
                loadingDialog.cancel();
            }
        });

    }
}