package ir.jozveban.market.activities;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import ir.jozveban.market.Posts.PostsItem;
import ir.jozveban.market.R;
import ir.jozveban.market.Search.Search;
import ir.jozveban.market.Search.SearchItem;
import ir.jozveban.market.adapters.OnEndReached;
import ir.jozveban.market.adapters.OnRecyclerItemClicked;
import ir.jozveban.market.adapters.PostsAdapter;

import com.github.ybq.android.spinkit.SpinKitView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.adapters.SearchAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity {

    @BindView(R.id.recycler_posts)
    RecyclerView recyclerPosts;

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.loading_posts)
    SpinKitView loadingPosts;

    private int pageNumber = 1;

    private SearchAdapter adapter;
    private List<SearchItem> posts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        posts = new ArrayList<>();
        adapter = new SearchAdapter(context, posts);
        recyclerPosts.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerPosts.setAdapter(adapter);

        searchView.setIconified(false);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
//                Toast.makeText(context,query,Toast.LENGTH_SHORT).show();
                posts.clear();
                pageNumber = 1;
                adapter.notifyDataSetChanged();
                search(query, pageNumber);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });
        adapter.setOnRecyclerItemClicked(new OnRecyclerItemClicked() {
            @Override
            public void OnClick(int position) {
                Intent intent = new Intent(context, PostActivity.class);
                intent.putExtra("postId", posts.get(position).getId());
                intent.putExtra("postTitle", posts.get(position).getTitle());
                startActivity(intent);
            }
        });

    }

    public void search(String str, int page) {
//        loadingPosts.setVisibility(View.VISIBLE);
        loadingDialog.show();
        aPi.searchPosts(str, 12, page).enqueue(
                new Callback<List<SearchItem>>() {
                    @Override
                    public void onResponse(Call<List<SearchItem>> call, Response<List<SearchItem>> response) {
                        if (response.code() == 200) {

                            if (response.body().size() != 0) {
                                posts.addAll(response.body());
                                adapter.notifyItemInserted(posts.size());

                                adapter.setOnEndReached(new OnEndReached() {
                                    @Override
                                    public void OnReached() {
                                        search(str, pageNumber);
                                    }
                                });

                                pageNumber++;
                            } else if (pageNumber == 1) {
                                Toast.makeText(context, "موردی یافت نشد", Toast.LENGTH_LONG)
                                        .show();
                            }


                        }
                        loadingDialog.cancel();
                    }

                    @Override
                    public void onFailure(Call<List<SearchItem>> call, Throwable t) {

                    }
                }
        );
    }
}