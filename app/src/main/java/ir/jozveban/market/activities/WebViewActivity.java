package ir.jozveban.market.activities;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;

public class WebViewActivity extends BaseActivity {

    Bundle bundle;


    @BindView(R.id.webview_content)
    WebView webView;
    @BindView(R.id.text_title)
    TextView textTitle;

    private DownloadManager mgr;
    boolean FLAG = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {

            bundle = getIntent().getExtras();
            textTitle.setText(bundle.getString("title"));

            loadWebView(bundle.getString("downloadId"),
                    bundle.getString("link"));

        }
        mgr = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);

    }

    public void loadWebView(String downloadId, String link) {

        Toast.makeText(context, "درحال بارگذاری", Toast.LENGTH_SHORT).show();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webView.setLongClickable(true);
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            startDownload(url);
        });

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);
        cookieManager.setAcceptThirdPartyCookies(webView, true);
        cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
            @Override
            public void onReceiveValue(Boolean aBoolean) {

            }
        });

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("download_id", "107493");
            jsonObject.put("edd_redirect_to_checkout", "1");
            jsonObject.put("edd_action", "add_to_cart");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String str = "download_id=" + downloadId + "&edd_redirect_to_checkout=1&edd_action=add_to_cart";
        webView.postUrl(link, str.getBytes());
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.contains("pay.ir") || url.contains("zarinpal.com") || url.contains("shaparak")) {
                    textTitle.setText(url);
                } else {
                    textTitle.setText(bundle.getString("title"));
                }
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                System.err.println(url);
                webView.loadUrl(url);
//                if (url.contains("pay.ir") || url.contains("zarinpal.com") || url.contains("shaparak")) {
//                    textTitle.setText(url);
//                } else {
//                    textTitle.setText(bundle.getString("title"));
//                }

                return true;

            }
        });

    }

    public void startDownload(String url) {
//        Uri uri = Uri.parse(url);
        Log.e(TAG, "startDownload: " + url);
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        fileName = fileName.substring(0, 1).toUpperCase() + fileName.substring(1);
//
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)// Uri of the destination file
                .setTitle(fileName)// Title of the Download Notification
                .setDescription("دانلود از جزوه بان")// Description of the Download Notification
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true);// Set if download is allowed on roaming network

        DownloadManager downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);// e
    }
}