package ir.jozveban.market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ir.jozveban.market.Categories.CategoriesItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.Holder> {

    private Context context;
    private List<CategoriesItem> categories;
    private OnRecyclerItemClicked onRecyclerItemClicked;
    private OnEndReached onEndReached;

    public CategoriesAdapter(List<CategoriesItem> categories, Context context) {
        this.categories = categories;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.row_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        CategoriesItem cat = categories.get(position);
        holder.button.setText(cat.getName());

        if (position == categories.size() - 1) {
            onEndReached.OnReached();
        }

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setOnRecyclerItemClicked(OnRecyclerItemClicked onRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked;
    }

    public void setOnEndReached(OnEndReached onEndReached) {
        this.onEndReached = onEndReached;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.button)
        Button button;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }
}
