package ir.jozveban.market.adapters;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ir.jozveban.market.Comments.CommentsItem;
import ir.jozveban.market.R;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.Holder> {

    List<CommentsItem> comments = new ArrayList<>();

    public CommentsAdapter(List<CommentsItem> comments) {
        this.comments = comments;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_comments, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        CommentsItem comment = comments.get(position);
        holder.authorName.setText(comment.getAuthorName());
        holder.commentContent.setText(Html.fromHtml(comment.getContent().getRendered()));


    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView imageAuthor;
        TextView authorName, commentContent;

        public Holder(@NonNull View itemView) {
            super(itemView);
            imageAuthor = itemView.findViewById(R.id.image_author);
            authorName = itemView.findViewById(R.id.text_author_name);
            commentContent = itemView.findViewById(R.id.text_content);

        }
    }
}
