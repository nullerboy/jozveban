package ir.jozveban.market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.Download.DownloadsItem;
import ir.jozveban.market.Downloads.Download;
import ir.jozveban.market.R;

public class DownloadsAdapter extends RecyclerView.Adapter<DownloadsAdapter.Holder> {

    private Context context;
    private List<DownloadsItem> downloads;

    OnRecyclerItemClicked onRecyclerItemClicked;

    public DownloadsAdapter(Context context, List<DownloadsItem> downloads) {
        this.context = context;
        this.downloads = downloads;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_download, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        DownloadsItem download = downloads.get(position);
        holder.textName.setText(download.getName());
        if (download.getFileUrl()==null){
            holder.textStatus.setText("پرداخت نشده");
        }else{
            holder.textStatus.setText("دانلود");
        }
    }

    @Override
    public int getItemCount() {
        return downloads.size();
    }

    public OnRecyclerItemClicked getOnRecyclerItemClicked() {
        return onRecyclerItemClicked;
    }

    public void setOnRecyclerItemClicked(OnRecyclerItemClicked onRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked;
    }


    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_status)
        TextView textStatus;

        @BindView(R.id.text_name)
        TextView textName;

        @BindView(R.id.card_row)
        CardView cardView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }
}
