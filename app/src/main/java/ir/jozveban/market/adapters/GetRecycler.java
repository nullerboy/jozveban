package ir.jozveban.market.adapters;

import androidx.recyclerview.widget.RecyclerView;

public interface GetRecycler {
    void getRecyclerView(RecyclerView recyclerView, int position);
}
