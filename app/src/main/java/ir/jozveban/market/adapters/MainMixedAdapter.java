package ir.jozveban.market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ir.jozveban.market.Categories.CategoriesItem;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;

public class MainMixedAdapter extends RecyclerView.Adapter<MainMixedAdapter.Holder> {

    private Context context;
    private List<CategoriesItem> categories;
    private OnRecyclerItemClicked onRecyclerItemClicked;
    private GetRecycler getRecycler;

    public MainMixedAdapter(Context context, List<CategoriesItem> categories) {
        this.context = context;
        this.categories = categories;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.row_main_mixed, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        CategoriesItem category = categories.get(position);

        holder.textTitle.setText(category.getName());
        holder.recyclerPosts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));

        getRecycler.getRecyclerView(holder.recyclerPosts, position);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setOnRecyclerItemClicked(OnRecyclerItemClicked onRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked;
    }

    public void getRecycler(GetRecycler getRecycler) {
        this.getRecycler = getRecycler;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_title)
        TextView textTitle;

        @BindView(R.id.recycler_posts)
        RecyclerView recyclerPosts;

        @BindView(R.id.button_see_more)
        Button buttonSeeMore;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            buttonSeeMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClicked.OnClick(getAdapterPosition());
                }
            });
        }
    }

}
