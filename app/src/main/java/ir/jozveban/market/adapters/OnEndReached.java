package ir.jozveban.market.adapters;

public interface OnEndReached {
    void OnReached();
}
