package ir.jozveban.market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import ir.jozveban.market.Posts.PostsItem;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.Holder> {

    private Context context;
    private List<PostsItem> posts;
    private OnRecyclerItemClicked onRecyclerItemClicked;
    private OnEndReached onEndReached;

    public PostsAdapter(Context context, List<PostsItem> posts) {
        this.context = context;
        this.posts = posts;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.row_post, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        PostsItem post = posts.get(position);

        if (post.getEmbedded()
                .getWpFeaturedmedia() != null) {
            Picasso.get().load(post.getEmbedded()
                    .getWpFeaturedmedia()
                    .get(0)
                    .getSourceUrl())
                    .into(holder.imagePosts);
        } else {
            holder.imagePosts.setImageResource(R.drawable.outline_no_photography_grey_500_24dp);
            holder.imagePosts.setScaleType(ImageView.ScaleType.CENTER);

        }
        holder.textTitle.setText(post.getTitle().getRendered());

        if (position == posts.size() - 1) {
            onEndReached.OnReached();
        }

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void setOnRecyclerItemClicked(OnRecyclerItemClicked onRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked;
    }

    public void setOnEndReached(OnEndReached onEndReached) {
        this.onEndReached = onEndReached;
    }


    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_title)
        TextView textTitle;

        @BindView(R.id.image_post)
        ImageView imagePosts;

        @BindView(R.id.card_post)
        CardView cardPost;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            cardPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClicked.OnClick(getAdapterPosition());
                }
            });

        }
    }
}
