package ir.jozveban.market.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;
import java.util.zip.Inflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import ir.jozveban.market.R;
import ir.jozveban.market.Search.SearchItem;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.Holder> {

    private Context context;
    private List<SearchItem> searchItems;
    private OnRecyclerItemClicked onRecyclerItemClicked;
    private OnEndReached onEndReached;

    public SearchAdapter(Context context, List<SearchItem> searchItems) {
        this.context = context;
        this.searchItems = searchItems;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_search, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        SearchItem item = searchItems.get(position);
        holder.textTitle.setText(item.getTitle());

        if (position == searchItems.size() - 1) {
            onEndReached.OnReached();
        }

    }

    @Override
    public int getItemCount() {
        return searchItems.size();
    }

    public void setOnRecyclerItemClicked(OnRecyclerItemClicked onRecyclerItemClicked) {
        this.onRecyclerItemClicked = onRecyclerItemClicked;
    }

    public void setOnEndReached(OnEndReached onEndReached) {
        this.onEndReached = onEndReached;
    }


    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_title)
        TextView textTitle;

        @BindView(R.id.card_search)
        CardView cardView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerItemClicked.OnClick(getAdapterPosition());
                }
            });

        }
    }
}
