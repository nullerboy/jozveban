package ir.jozveban.market.util;

import ir.jozveban.market.Categories.CategoriesItem;
import ir.jozveban.market.Comments.CommentsItem;
import ir.jozveban.market.Me.Me;
import ir.jozveban.market.Post.Post;
import ir.jozveban.market.Posts.PostsItem;

import java.util.List;
import java.util.Map;

import ir.jozveban.market.Search.Search;
import ir.jozveban.market.Search.SearchItem;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APi {

    @GET("posts?_embed")
    Call<List<PostsItem>> getPosts(@Query("per_page") int perPage,
                                   @Query("page") int page);

    @GET("search?_embed")
    Call<List<SearchItem>> searchPosts(@Query("search") String search,
                                       @Query("per_page") int perPage,
                                       @Query("page") int page);

    @GET("posts?_embed")
    Call<List<PostsItem>> getPostsByCategory(@Query("per_page") int perPage,
                                             @Query("page") int page,
                                             @Query("categories") int catId);

    @GET("categories?_embed&per_page=100")
    Call<List<CategoriesItem>> getCategories();

    @GET("categories?_embed&per_page=100")
    Call<List<CategoriesItem>> getCategoriesByParent(@Query("parent") int parentId);

    @GET("categories?_embed&per_page=1")
    Call<ResponseBody> getCat();

    @GET("posts/{postId}?_embed")
    Call<Post> getPost(@Path("postId") int postId);

    @GET("users/me")
    Call<Me> login(@HeaderMap Map<String, String> headers);

    @FormUrlEncoded
    @POST("users")
    @Headers("Authorization: Basic YmFybmFtZTpiYXJuYW1lQDE0MDAyMzk5")
    Call<ResponseBody> register(@Field("email") String email,
                                @Field("username") String username,
                                @Field("password") String password);


    @GET("comments/")
    Call<List<CommentsItem>> getComments(@Query("post") int postId);

    @FormUrlEncoded
    @POST("comments/")
    Call<ResponseBody> submitComment(@HeaderMap Map<String, String> headers,
                                     @Field("post") int postId,
                                     @Field("author_name") String authorName,
                                     @Field("author_email") String authorEmail,
                                     @Field("content") String commentContent);


}
