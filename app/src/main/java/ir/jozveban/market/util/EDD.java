package ir.jozveban.market.util;

import java.util.List;

import ir.jozveban.market.Download.DownloadsItem;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EDD {

    @GET("sales")
    Call<List<DownloadsItem>> getDownloads(@Query("token") String token,
                                           @Query("key") String key,
                                           @Query("user_id") String userId);

}
