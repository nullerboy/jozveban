package ir.jozveban.market.util;

public class Params {
    public static String MAIN_BASE_URL = "https://jozveban.ir/";
    public static String BASE_URL = MAIN_BASE_URL + "wp-json/wp/v2/";
    public static String BASE_URL_EDD = MAIN_BASE_URL + "edd-api/v2/";
    public static String KEY = "cd0212d3670d43e793b2e70e35d93db8";
    public static String TOKEN = "5878d9d59fc3ae8225d4363adf7e7748";
    public static String LINK_SUPPORT = "https://jozveban.ir/po/";
    public static String LINK_TERMS = "https://jozveban.ir/term-of-services/";
    public static String LINK_OTHER_APPS = "https://cafebazaar.ir/developer/jozveban";
    public static String LINK_JOZVEBAN_APP = "https://cafebazaar.ir/app/ir.jozveban.market";
    public static String AUTH = "Basic YmFybmFtZTpiYXJuYW1lQDE0MDAyMzk5";
    public static int NEW_POSTS_CATEGORY_ID = 468;
    public static int FEATURED_CATEGORY_ID = 1011;
    public static int MAIN_CATEGORY_PARENT_CATEGORY_ID = 1160;
    public static int MAIN_MIX_PARENT_CATEGORY_ID = 468;

    public static int CAT_ID_1 = 1011;
    public static int CAT_ID_2 = 868;

}
