package ir.jozveban.market.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Pref {

    private Context context;
    private SharedPreferences preferences;

    private final static String CONFIG_FILE = "config";
    private final static String USER_ID = "userId";
    private final static String USER_CREDENTIAL = "credential";
    private final static String USER_EMAIL = "email";

    public Pref(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(CONFIG_FILE, Context.MODE_PRIVATE);
    }

    public String getUserEmail() {
        return preferences.getString(USER_EMAIL, "");
    }

    public void setUserEmail(String email){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_EMAIL, email);
        editor.apply();
    }



    public String getUserId() {
        return preferences.getString(USER_ID, "");
    }

    public String getUserCredential() {
        return preferences.getString(USER_CREDENTIAL, "YmFybmFtZTpCYmFybmFtZUAxMjM0NQ==");
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_ID, userId);
        editor.apply();
    }

    public void setUserCredential(String credential) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_CREDENTIAL, credential);
        editor.apply();
    }

    public void clearPrefs() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(USER_ID);
        editor.remove(USER_CREDENTIAL);
        editor.remove(USER_EMAIL);
        editor.apply();
    }

}
