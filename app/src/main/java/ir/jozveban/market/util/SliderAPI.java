package ir.jozveban.market.util;

import java.util.List;

import ir.jozveban.market.Slider.ResponseSliderItem;
import retrofit2.Call;
import retrofit2.http.GET;

public interface SliderAPI {
    @GET("app-slider/data/data.json")
    Call<List<ResponseSliderItem>> getSlider();
}
